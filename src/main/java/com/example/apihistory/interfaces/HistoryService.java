package com.example.apihistory.interfaces;

import java.util.List;

import com.example.apihistory.dto.HistoryDTO;

public interface HistoryService {
	
	Boolean addHistory(HistoryDTO history);

	HistoryDTO getId(Integer idSalida);

	Boolean updatehistory(Integer idSalida, HistoryDTO history);

	Boolean deleteId(Integer idSalida);

	List<HistoryDTO> getAllHistory();
}
