package com.example.apihistory.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.apihistory.dto.HistoryDTO;
import com.example.apihistory.entities.History;
import com.example.apihistory.repositories.HistoryRepository;
import com.example.apihistory.interfaces.HistoryService;

@Service
public class HistoryServicelmpl implements HistoryService {

	@Autowired
	private HistoryRepository repository;

	@Override
	public Boolean addHistory(HistoryDTO history) {
		History entity = this.getHistoryEntity(history);
			this.repository.insert(entity);
			return true;
	}

	private History getHistoryEntity(HistoryDTO history) {
		
		History response = new History();
		
		response.setIdSalida(history.getIdSalida());
		response.setIdConductor(history.getIdConductor());
		response.setIdCamion(history.getIdCamion());
		response.setFechaSalida(history.getFechaSalida());
		return response;
	}

	@Override
	public Boolean updatehistory(Integer id, HistoryDTO history) {
		Optional<History> optionalhistory = this.repository.findById(id);
		if(optionalhistory.isPresent()) {
			History entity = getUpdateHistoryEntity(history, id);
			this.repository.save(entity);
			return true;
		}
		return false;
	}

	private History getUpdateHistoryEntity(HistoryDTO history, Integer id) {
		History response = new History();
		
		response.setIdSalida(id);
		response.setIdConductor(history.getIdConductor());
		response.setIdCamion(history.getIdCamion());
		response.setFechaSalida(history.getFechaSalida());
		return response;
	}

	@Override
	public Boolean deleteId(Integer id) {
		Optional<History> optionalhistory = this.repository.findById(id);
		if(optionalhistory.isPresent()) {
			History entity = optionalhistory.get();
			this.repository.delete(entity);
			return true;
		}
		return false;
	}

	@Override
	public List<HistoryDTO> getAllHistory() {
		List<History> listhistory = this.repository.findAll();
		if(listhistory != null) {
			List<HistoryDTO> response = new ArrayList<HistoryDTO>();
			for(History item : listhistory) {
				response.add(this.getAllHistoryDTO(item));
			}
			return response;
		}
		return null;
	}

	private HistoryDTO getAllHistoryDTO(History item) {
		HistoryDTO response = new HistoryDTO();
		response.setIdSalida(item.getIdSalida());
		response.setIdConductor(item.getIdConductor());
		response.setIdCamion(item.getIdCamion());
		response.setFechaSalida(item.getFechaSalida());
		
		return response;
	}

	@Override
	public HistoryDTO getId(Integer id) {
		Optional<History> optionalhistory = this.repository.findById(id);
		if(optionalhistory.isPresent()) {
			History entity = optionalhistory.get();
			HistoryDTO response = this.getHistoryDTO(entity);
			return response;
		}
		return null;
	}

	private HistoryDTO getHistoryDTO(History entity) {
		
		HistoryDTO response = new HistoryDTO();
		
		response.setIdSalida(entity.getIdSalida());
		response.setIdConductor(entity.getIdConductor());
		response.setIdCamion(entity.getIdCamion());
		response.setFechaSalida(entity.getFechaSalida());
		
		return response;
	}
}
