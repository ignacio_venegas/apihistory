package com.example.apihistory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApihistoryApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApihistoryApplication.class, args);
	}

}
