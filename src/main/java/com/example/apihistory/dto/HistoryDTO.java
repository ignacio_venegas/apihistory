package com.example.apihistory.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HistoryDTO {
	private Integer idSalida;
	private Integer idConductor;
	private Integer idCamion;
	private String fechaSalida;
}
