package com.example.apihistory.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.apihistory.dto.HistoryDTO;
import com.example.apihistory.interfaces.HistoryService;

@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
@RestController
@RequestMapping("/apihistory")
public class HistoryController {
	
	@Autowired
	private HistoryService historyService;
	
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Boolean> addhistory(@RequestBody HistoryDTO history){
		Boolean response = this.historyService.addHistory(history);
		return new ResponseEntity<Boolean>(response,HttpStatus.ACCEPTED);
	}
	
	@GetMapping(value = "/{idSalida}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<HistoryDTO> getIdSalida(@PathVariable("idSalida") Integer idSalida){
		HistoryDTO response = this.historyService.getId(idSalida);
		return new ResponseEntity<HistoryDTO>(response, HttpStatus.OK);
	}
	
	@PutMapping(value = "/{idSalida}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Boolean> updatehistory(@PathVariable("idSalida") Integer idSalida, @RequestBody HistoryDTO history){
		Boolean response = this.historyService.updatehistory(idSalida, history);
		return new ResponseEntity<Boolean>(response, HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/{idSalida}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Boolean> deleteId(@PathVariable("idSalida") Integer idSalida){
		Boolean response = this.historyService.deleteId(idSalida);
		return new ResponseEntity<Boolean>(response, HttpStatus.OK);
	}
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<HistoryDTO>> getAllHistory(){
		List<HistoryDTO> response = this.historyService.getAllHistory();
		return new ResponseEntity<List<HistoryDTO>>(response, HttpStatus.OK);
	}
}
