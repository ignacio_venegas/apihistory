package com.example.apihistory.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.example.apihistory.entities.History;

@Repository
public interface HistoryRepository extends MongoRepository<History, Integer>{

}
