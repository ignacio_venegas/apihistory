package com.example.apihistory.entities;

import org.springframework.data.annotation.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class History {

	@Id
	private Integer idSalida;
	private Integer idConductor;
	private Integer idCamion;
	private String fechaSalida;
}
